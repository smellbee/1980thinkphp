<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link rel='stylesheet' type="text/css" href="__PUBLIC__/css/style.css" />
<script type="text/javascript" src="__PUBLIC__/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="__PUBLIC__/js/common.js"></script>
 <script language="JavaScript">
        <!--
        var URL = '__URL__';
        var APP	 = '__APP__';
        var SELF='__SELF__';
        var PUBLIC='__PUBLIC__';
        //-->
        </script>
</head>
<body>
<div class="main">
    <div class="pos"><?php echo ($type); ?>
    </div>

    <div class="operate">
        <div class="left">
            <?php if(ACTION_NAME == "index"): ?><input type="button" onclick="doGoBatch('<?php echo U(GROUP_NAME. '/Database/backup');?>')" class="btn_blue" value="数据库备份">
                <input type="button" onclick="doGoBatch('<?php echo U(GROUP_NAME. '/Database/optimize', array('batchFlag' => 1));?>')" class="btn_blue" value="数据表优化">
                <input type="button" onclick="doGoBatch('<?php echo U(GROUP_NAME. '/Database/repair', array('batchFlag' => 1));?>')" class="btn_blue" value="数据表修复">
                <input type="button" onclick="goUrl('<?php echo U(GROUP_NAME.'/Database/restore');?>')" class="btn_green" value="还原管理">
            <?php else: ?>
                <input type="button" onclick="goUrl('<?php echo U(GROUP_NAME. '/Database/index');?>')" class="btn_blue" value="返回">
                <input type="button" onclick="doGoBatch('<?php echo U(GROUP_NAME.'/Database/restore');?>')" class="btn_green" value="还原">
                <input type="button" onclick="doConfirmBatch('<?php echo U(GROUP_NAME.'/Database/clear');?>', '确实要彻底删除选择项吗？')" class="btn_orange" value="彻底删除"><?php endif; ?>


            
        </div>
    </div>
    <div class="list">    
    <form action="<?php echo U(GROUP_NAME. '/Database/backup');?>" method="post" id="form_do" name="form_do">
        <table width="100%">
            <tr>
                <th><input type="checkbox" id="check"></th>
                <th>表名</th>
                <th>表用途</th>
                <th>记录行数</th>
                <th>引擎</th>
                <th>字符集</th>
                <th>表大小</th>
                <th>操作</th>
            </tr>
			<?php if(is_array($vlist)): foreach($vlist as $key=>$v): ?><tr>
                <td><input type="checkbox" name="key[]" value="<?php echo ($v["Name"]); ?>"></td>
                <td class="aleft"><?php echo ($v["Name"]); ?></td>
                <td><?php echo ($v["Comment"]); ?></td>
                <td><?php echo ($v["Rows"]); ?></td>
                <td><?php echo ($v["Engine"]); ?></td>
                <td><?php echo ($v["Collation"]); ?></td>
                <td><?php echo ($v["size"]); ?></td>
                <td>
                <a href="<?php echo U(GROUP_NAME. '/Database/optimize',array('tablename' => $v['Name']), '');?>">优化</a>
                <a href="<?php echo U(GROUP_NAME. '/Database/repair',array('tablename' => $v['Name']), '');?>">修复</a>
                <!--a href="<?php echo U(GROUP_NAME. '/Database/repair',array('tablename' => $v['Name']), '');?>">结构</a-->            
				</td>
            </tr><?php endforeach; endif; ?>
        </table>
        <div class="th" style="clear: both;">数据库中共有<?php echo ($tableNum); ?>张表，共计<?php echo ($total); ?></div>
    </form>
    </div>
</div>
</body>
</html>