<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link rel='stylesheet' type="text/css" href="__PUBLIC__/css/style.css" />
<script type="text/javascript" src="__PUBLIC__/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="__PUBLIC__/js/common.js"></script>
<script type="text/javascript" src="__PUBLIC__/js/jquery.form.min.js"></script>

<script type="text/javascript">
	window.UEDITOR_HOME_URL = '__DATA__/ueditor/';
	window.onload = function() {
		window.UEDITOR_CONFIG.initialFrameWidth=600;
		window.UEDITOR_CONFIG.initialFrameHeight=300;
		//图片上传配置区
		window.UEDITOR_CONFIG.imageUrl = "<?php echo U(GROUP_NAME. '/Public/upload');?>" ;   //图片上传提交地址
		window.UEDITOR_CONFIG.imagePath = "" ; //图片修正地址，引用了fixedImagePath,可自行配置      
		window.UEDITOR_CONFIG.imageManagerUrl = "<?php echo U(GROUP_NAME. '/Public/getFileOfImg');?>" ;////图片在线管理的处理地址
		window.UEDITOR_CONFIG.imageManagerPath = ""; //图片在线管理修正地址  
			UE.getEditor('content');

	}
</script>
<script type="text/javascript" src="__DATA__/ueditor/ueditor.config.js"></script>
<script type="text/javascript" src="__DATA__/ueditor/ueditor.all.min.js"></script>	
<script type="text/javascript">
$(function () {



	//图片集上传
	var picture_show = $('#picture_show');
	var picture_tip = $(".picture_tip");
	$("#picture_upload").wrap("<form id='picture_form' action='<?php echo U(GROUP_NAME. '/Public/upload');?>' method='post' enctype='multipart/form-data'></form>");
    $("#picture_upload").change(function(){
    	if($("#picture_upload").val() == "") return;    
    	if ($("#picture_show>.picture_item").length >=5 ) {alert('产品最多上传5张');return;}
    	
		$("#picture_form").ajaxSubmit({
			dataType:  'json',
			beforeSend: function() {
				picture_tip.html("上传中...");
    		},
			success: function(data) {
				//picture_tip.html("<b>"+data.title+"("+data.size+"k)</b> <span class='delimg' rel='"+data.url+"'>删除</span>");
				if(data.state == 'SUCCESS'){	
					picture_tip.html(""+ data.title +" 上传成功("+data.size+"k)");					
					var img = data.url;
					var picture_html= '<div class="picture_item"><img src="'+ img +'" width="120"><div class="picture_del">删除</div><div class="picture_go_up">前移</div><input type="hidden" name="pictureurls[]" value="'+ img +'" /></div>'
					picture_show.append(picture_html);
					//picture_show.parent().find("span").remove().end().append("<span class='error'>描述不能为空</span>");
				}else {
					picture_tip.html(data.state);	
				}			

			},
			error:function(xhr){
				picture_tip.html("上传失败"+xhr);

			}
		});
	});
    //图片集上传
    var picture_show_small = $('#picture_show_small');
    var picture_tip_small = $(".picture_tip_small");
    $("#picture_upload_small").wrap("<form id='picture_form_small' action='<?php echo U(GROUP_NAME. '/Public/upload');?>' method='post' enctype='multipart/form-data'></form>");
    $("#picture_upload_small").change(function(){
        if($("#picture_upload_small").val() == "") return;
        if ($("#picture_show_small>.picture_item").length >=1 ) {alert('产品最多上传1张');return;}

        $("#picture_form_small").ajaxSubmit({
            dataType:  'json',
            beforeSend: function() {
                picture_tip_small.html("上传中...");
            },
            success: function(data) {
                //picture_tip.html("<b>"+data.title+"("+data.size+"k)</b> <span class='delimg' rel='"+data.url+"'>删除</span>");
                if(data.state == 'SUCCESS'){
                    picture_tip_small.html(""+ data.title +" 上传成功("+data.size+"k)");
                    var img = data.url;
                    var picture_html= '<div class="picture_item picture_item_small"><img src="'+ img +'" width="120"><div class="picture_del">删除</div><div class="picture_go_up">前移</div><input type="hidden" name="picsmall" value="'+ img +'" /></div>'
                    picture_show_small.append(picture_html);
                    //picture_show.parent().find("span").remove().end().append("<span class='error'>描述不能为空</span>");
                }else {
                    picture_tip_small.html(data.state);
                }

            },
            error:function(xhr){
                picture_tip_small.html("上传失败"+xhr);

            }
        });
    });
    //图片集上传
    var picture_show_long = $('#picture_show_long');
    var picture_tip_long = $(".picture_tip_long");
    $("#picture_upload_long").wrap("<form id='picture_form_long' action='<?php echo U(GROUP_NAME. '/Public/upload');?>' method='post' enctype='multipart/form-data'></form>");
    $("#picture_upload_long").change(function(){
        if($("#picture_upload_long").val() == "") return;
        if ($("#picture_show_long>.picture_item").length >=1 ) {alert('产品最多上传1张');return;}

        $("#picture_form_long").ajaxSubmit({
            dataType:  'json',
            beforeSend: function() {
                picture_tip_long.html("上传中...");
            },
            success: function(data) {
                //picture_tip.html("<b>"+data.title+"("+data.size+"k)</b> <span class='delimg' rel='"+data.url+"'>删除</span>");
                if(data.state == 'SUCCESS'){
                    picture_tip_long.html(""+ data.title +" 上传成功("+data.size+"k)");
                    var img = data.url;
                    var picture_html= '<div class="picture_item picture_item_long"><img src="'+ img +'" width="120"><div class="picture_del">删除</div><div class="picture_go_up">前移</div><input type="hidden" name="piclong" value="'+ img +'" /></div>'
                    picture_show_long.append(picture_html);
                    //picture_show.parent().find("span").remove().end().append("<span class='error'>描述不能为空</span>");
                }else {
                    picture_tip_long.html(data.state);
                }

            },
            error:function(xhr){
                picture_tip.html("上传失败"+xhr);

            }
        });
    });
    //图片集上传
    var picture_show_big = $('#picture_show_big');
    var picture_tip_big = $(".picture_tip_big");
    $("#picture_upload_big").wrap("<form id='picture_form_big' action='<?php echo U(GROUP_NAME. '/Public/upload');?>' method='post' enctype='multipart/form-data'></form>");
    $("#picture_upload_big").change(function(){
        if($("#picture_upload_big").val() == "") return;
        if ($("#picture_show_big>.picture_item").length >=1 ) {alert('产品最多上传1张');return;}

        $("#picture_form_big").ajaxSubmit({
            dataType:  'json',
            beforeSend: function() {
                picture_tip_big.html("上传中...");
            },
            success: function(data) {
                //picture_tip.html("<b>"+data.title+"("+data.size+"k)</b> <span class='delimg' rel='"+data.url+"'>删除</span>");
                if(data.state == 'SUCCESS'){
                    picture_tip_big.html(""+ data.title +" 上传成功("+data.size+"k)");
                    var img = data.url;
                    var picture_html= '<div class="picture_item picture_item_big"><img src="'+ img +'" width="120"><div class="picture_del">删除</div><div class="picture_go_up">前移</div><input type="hidden" name="picbig" value="'+ img +'" /></div>'
                    picture_show_big.append(picture_html);
                    //picture_show.parent().find("span").remove().end().append("<span class='error'>描述不能为空</span>");
                }else {
                    picture_tip_big.html(data.state);
                }

            },
            error:function(xhr){
                picture_tip_big.html("上传失败"+xhr);

            }
        });
    });
    //图片集上传
    var picture_show_detail = $('#picture_show_detail');
    var picture_tip_detail = $(".picture_tip_detail");
    $("#picture_upload_detail").wrap("<form id='picture_form_detail' action='<?php echo U(GROUP_NAME. '/Public/upload');?>' method='post' enctype='multipart/form-data'></form>");
    $("#picture_upload_detail").change(function(){
        if($("#picture_upload_detail").val() == "") return;
        if ($("#picture_show_detail>.picture_item").length >=1 ) {alert('产品最多上传1张');return;}

        $("#picture_form_detail").ajaxSubmit({
            dataType:  'json',
            beforeSend: function() {
                picture_tip_detail.html("上传中...");
            },
            success: function(data) {
                //picture_tip.html("<b>"+data.title+"("+data.size+"k)</b> <span class='delimg' rel='"+data.url+"'>删除</span>");
                if(data.state == 'SUCCESS'){
                    picture_tip_detail.html(""+ data.title +" 上传成功("+data.size+"k)");
                    var img = data.url;
                    var picture_html= '<div class="picture_item picture_item_detail"><img src="'+ img +'" width="120"><div class="picture_del">删除</div><div class="picture_go_up">前移</div><input type="hidden" name="picdetail" value="'+ img +'" /></div>'
                    picture_show_detail.append(picture_html);
                    //picture_show.parent().find("span").remove().end().append("<span class='error'>描述不能为空</span>");
                }else {
                    picture_tip_detail.html(data.state);
                }

            },
            error:function(xhr){
                picture_tip_detail.html("上传失败"+xhr);

            }
        });
    });
	
});

$(function () {
	//jquery1.7后没有live
	//$(document).on 也行
	$('.picture_item').on('mouseenter',function(){
			$(this).find('.picture_go_up').show();
			$(this).find('.picture_del').show();
		}).on('mouseleave',function(){
			$(this).find('.picture_go_up').hide();
			$(this).find('.picture_del').hide();
		}
	); 

    $('.picture_go_up').on('click',function () {
        var parent = $(this).parent();
        if (parent.index() == 0){
        }else{
        	parent.prev().before(parent);
        	$(this).hide();
        	parent.find('.picture_del').hide();
        } 
        
    });

    $('.picture_del').on('click',function () {
        var img = $(this).next().val();//下个元素input的值 
		$(this).parent().remove();////移除父元素

		return;
		$.post("action.php?act=delimg",{imagename:pic},function(msg){
			if(msg==1){
				litpic_show.empty();
			}else{
				alert(msg);
			}
		});        
    });


	$('#CK_JumpUrl').click(function(){
            var inputs = $(this).parents('dl').find('input');
            if($(this).attr('checked')) {
                $('#JumpUrlDiv').show();

            }else {
                $('#JumpUrlDiv').hide();
            }
            
     });

});
</script>
</head>
<body>
<div class="main">
    <div class="pos">修改产品</div>
	<div class="form">
		<form method='post' id="form_do" name="form_do" action="<?php echo U(GROUP_NAME. '/Product/edit');?>">
		<dl>
			<dt> 标题：</dt>
			<dd>
				<input type="text" name="title" class="inp_large" value="<?php echo ($vo["title"]); ?>" />
			</dd>
		</dl>
		<dl>
			<dt> 自定义属性：</dt>
			<dd>
				<?php if(is_array($flagtypelist)): foreach($flagtypelist as $key=>$v): ?><label><input type='checkbox' name='flags[]' value='<?php echo ($key); ?>' <?php if($key == B_JUMP): ?>id="CK_JumpUrl"<?php endif; ?> <?php if(($vo["flag"] & $key) == $key): ?>checked="checked"<?php endif; ?> /> <?php echo ($v); ?></label>&nbsp;<?php endforeach; endif; ?>
			</dd>
		</dl>

		<dl id="JumpUrlDiv" <?php if(($vo["flag"] & B_JUMP) == 0): ?>style="display:none;"<?php endif; ?>>
			<dt> 跳转网址：</dt>
			<dd>
				<input type="text" name="jumpurl" class="inp_large" value="<?php echo ($vo["jumpurl"]); ?>" />
			</dd>
		</dl>
		<dl>
			<dt> 所属栏目：</dt>
			<dd>
				<select name="cid">
					<?php if(is_array($cate)): foreach($cate as $key=>$v): ?><option value="<?php echo ($v["id"]); ?>" <?php if($vo["cid"] == $v['id']): ?>selected="selected"<?php endif; ?>><?php echo ($v["delimiter"]); echo ($v["name"]); ?></option><?php endforeach; endif; ?>
				</select>
			</dd>
		</dl>
            <dl>
                <dt> 产品小图：</dt>
                <dd>
                    <div class="litpic_show litpic_show_small">
                        <div class="btn_up">
                            <span>上传图片</span>
                            <input id="picture_upload_small" type="file" name="mypic1">
                        </div>
                        <div class="picture_tip picture_tip_small"></div>
                        <div id="picture_show_small">
                            <?php echo ($small); ?>
                        </div>
                    </div>
                </dd>
            </dl>
            <dl>
                <dt> 产品中图：</dt>
                <dd>
                    <div class="litpic_show litpic_show_long">
                        <div class="btn_up">
                            <span>上传图片</span>
                            <input id="picture_upload_long" type="file" name="mypic1">
                        </div>
                        <div class="picture_tip picture_tip_long"></div>
                        <div id="picture_show_long">
                            <?php echo ($long); ?>
                        </div>
                    </div>
                </dd>
            </dl>
            <dl>
                <dt> 产品大图：</dt>
                <dd>
                    <div class="litpic_show litpic_show_big">
                        <div class="btn_up">
                            <span>上传图片</span>
                            <input id="picture_upload_big" type="file" name="mypic1">
                        </div>
                        <div class="picture_tip picture_tip_big"></div>
                        <div id="picture_show_big">
                            <?php echo ($big); ?>
                        </div>
                    </div>
                </dd>
            </dl>
            <dl>
                <dt> 产品详情图：</dt>
                <dd>
                    <div class="litpic_show litpic_show_detail">
                        <div class="btn_up">
                            <span>上传图片</span>
                            <input id="picture_upload_detail" type="file" name="mypic1">
                        </div>
                        <div class="picture_tip picture_tip_detail"></div>
                        <div id="picture_show_detail">
                            <?php echo ($detail); ?>
                        </div>
                    </div>
                </dd>
            </dl>
		<dl>
			<dt> 产品图片：</dt>
			<dd>
				<div class="litpic_show">				    
					<div class="btn_up">
				        <span>上传多图</span>
				        <input id="picture_upload" type="file" name="mypic1">
				    </div>
				    <div class="picture_tip"></div>
				    <div id="picture_show">
				    <?php if(is_array($vo["pictureurls"])): foreach($vo["pictureurls"] as $key=>$value): ?><div class="picture_item"><img src="<?php echo ($value["url"]); ?>" width="120"><div class="picture_del">删除</div><div class="picture_go_up">前移</div><input type="hidden" name="pictureurls[]" value="<?php echo ($value["url"]); ?>" /></div><?php endforeach; endif; ?>
				    </div>
				</div>
			</dd>
		</dl>

		<dl>
			<dt> 关键词：</dt>
			<dd>
				<input type="text" name="keywords" class="inp_w250" value="<?php echo ($vo["keywords"]); ?>" />
			</dd>
		</dl>
		<dl>
			<dt> 摘要：</dt>
			<dd>
				<textarea name="description" class="tarea_default"><?php echo ($vo["description"]); ?></textarea>
			</dd>
		</dl>
		<dl>
			<dt> 价格：</dt>
			<dd>
				<input type="text" name="price" class="inp_one" value="<?php echo ($vo["price"]); ?>"/>
			</dd>
		</dl>

		<dl>
			<dt> 品牌：</dt>
			<dd>
				<input type="text" name="brand" class="inp_one" value="<?php echo ($vo["brand"]); ?>" />
			</dd>
		</dl>

		<dl>
			<dt> 单位：</dt>
			<dd>
				<input type="text" name="units" class="inp_one" value="<?php echo ($vo["units"]); ?>" />
			</dd>
		</dl>

		<dl>
			<dt> 规格：</dt>
			<dd>
				<input type="text" name="specification" class="inp_one" value="<?php echo ($vo["specification"]); ?>" />
			</dd>
		</dl>		
		<dl>
			<dt> 内容：</dt>
			<dd>
				<textarea name="content" id="content"><?php echo ($vo["content"]); ?></textarea>
			</dd>
		</dl>
		<dl>
			<dt> 评论：</dt>
			<dd>
				<input type="radio" name="commentflag" value="1" <?php if($vo["commentflag"] == 1): ?>checked="checked"<?php endif; ?> />允许 <input type="radio" name="commentflag" value="0" <?php if($vo["commentflag"] == 0): ?>checked="checked"<?php endif; ?> />禁止
			</dd>
		</dl>	
		<div class="form_b">
			<input type="hidden" name="id" value="<?php echo ($vo["id"]); ?>" />
			<input type="hidden" name="pid" value="<?php echo ($pid); ?>" />
			<input type="submit" class="btn_blue" id="submit" value="提 交">
		</div>
	   </form>
	</div>
</div>


</body>
</html>