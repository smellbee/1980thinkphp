<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo ($title); ?>-欢迎来到某某的公司</title>
    <meta name="keywords" content="<?php echo ($keywords); ?>" />
    <meta name="description" content="<?php echo ($description); ?>" />
    <script type="text/javascript" src="__PUBLIC__/js/jquery-1.7.2.min.js"></script>
    <link href="__PUBLIC__/css/style.css" rel="stylesheet" type="text/css" />
    <link href="__PUBLIC__/css/ccc.css" rel="stylesheet" type="text/css" />
</head>

<body>
<!--top -->
<div id="main">
    <div id="small-logo"><a class="small-logo-link left" href="http://127.0.0.1:1005"><img src="__PUBLIC__/images/icon-small-logo.jpg" title="1980特产"></a><a class="left goods-name"><?php echo $data[0]['tags']; ?></a></div>

    <div class="essay-header">
        <a class="essay-time left"><?php echo date("Y年m月d日",$data[0]['date']);?></a>
        <div class="essay-share right">
            <a class="left">分享到：</a>
            <div class="bdsharebuttonbox left"><a href="#" class="bds_more" data-cmd="more"></a><a href="#" class="bds_qzone" data-cmd="qzone" title="分享到QQ空间"></a><a href="#" class="bds_tsina" data-cmd="tsina" title="分享到新浪微博"></a><a href="#" class="bds_tqq" data-cmd="tqq" title="分享到腾讯微博"></a><a href="#" class="bds_renren" data-cmd="renren" title="分享到人人网"></a><a href="#" class="bds_weixin" data-cmd="weixin" title="分享到微信"></a></div>
            <script>
                window._bd_share_config={"common":{"bdSnsKey":{},"bdText":"","bdMini":"2","bdMiniList":false,"bdPic":"","bdStyle":"0","bdSize":"32"},"share":{}};with(document)0[(getElementsByTagName('head')[0]||body).appendChild(createElement('script')).src='http://bdimg.share.baidu.com/static/api/js/share.js?v=89860593.js?cdnversion='+~(-new Date()/36e5)];
            </script>
        </div>
    </div>

    <div class="essay-photo">
        <img src="<?php echo ($content["picdetail"]); ?>">
    </div>
    <div class="essay-font">
        <?php echo ($content["content"]); ?>
    </div>
    <!--
<div class="right f_r">
<h3 class="nybt"><i>您当前的位置：<?php
 $_sname = ""; $_typeid =-1; if($_typeid == -1) $_typeid = I('cid', 0, 'intval'); if ($_typeid == 0 && $_sname == '') { $_sname = isset($title) ? $title : ''; } echo getPosition($_typeid, $_sname, "", 0, ""); ?> </i><span><?php echo ($cate["name"]); ?></span></h3>
<div class="xbox wzzw">
<div class="biaoti" align="center"><?php echo ($content["title"]); ?></div>
<div style="border:1px solid #FFE0B4; background:#ffeed5; text-align:center;">发布时间:<?php echo (date('Y-m-d',$content["publishtime"])); ?> 丨 阅读次数:<?php
 if (!empty($id) && !empty($tablename)) { if (C('HTML_CACHE_ON') == true) { echo '<script type="text/javascript" src="'.U(GROUP_NAME. '/Public/click', array('id' => $id, 'tn' => $tablename)).'"></script>'; } else { echo getClick($id, $tablename); } } ?> </div>
<div class="wzzw lh">
<?php echo ($content["content"]); ?>
</div>
-->
    <!--
 <div class="fenye1" style="text-align:left;">↑上一篇：<?php
 if(empty($content['id']) || empty($content['cid']) || empty($cate['tablename']) ) { echo '无记录'; } else { $_vo=D(ucfirst($cate['tablename'].'View'))->where(array($cate['tablename'].'.status' => 0, 'id' => array('lt',$content['id'])))->order('id desc')->find(); if ($_vo) { $_jumpflag = ($_vo['flag'] & B_JUMP) == B_JUMP? true : false; $_vo['url'] = getContentUrl($_vo['id'], $_vo['cid'], $_vo['ename'], $_jumpflag, $_vo['jumpurl']); if(0) $_vo['title'] = str2sub($_vo['title'], 0, 0); echo '<a href="'. $_vo['url'] .'">'. $_vo['title'] .'</a>'; } else { echo '第一篇'; } } ?><br/>↓下一篇：<?php
 if(empty($content['id']) || empty($content['cid']) || empty($cate['tablename']) ) { echo '无记录'; } else { $_vo=D(ucfirst($cate['tablename'].'View'))->where(array($cate['tablename'].'.status' => 0,'id' => array('gt',$content['id'])))->order('id ASC')->find(); if ($_vo) { $_jumpflag = ($_vo['flag'] & B_JUMP) == B_JUMP? true : false; $_vo['url'] = getContentUrl($_vo['id'], $_vo['cid'], $_vo['ename'], $_jumpflag, $_vo['jumpurl']); if(0) $_vo['title'] = str2sub($_vo['title'], 0, 0); echo '<a href="'. $_vo['url'] .'">'. $_vo['title'] .'</a>'; } else { echo '最后一篇'; } } ?> </div>

    <!--comment
    <?php if($commentflag == 1): ?><div class="comment-box">
        <h3>评论(<span class="review-count">0</span>)</h3>
		
        <div class="more-comment">
            后面还有<span id="more_count"></span>条评论，<a href="javascript:get_review();">点击查看>></a>
        </div>
        <form action="<?php echo U(GROUP_NAME.'/Review/add');?>" method="post" class="comment-item" id="reviewForm"  autocomplete="off">
		<a name="reply_" id="reply_"></a>
        	<input type="hidden" name="post_id" value="<?php echo ($content["id"]); ?>"/>
        	<input type="hidden" name="model_id" value="<?php echo ($cate["modelid"]); ?>" />
        	<input type="hidden" name="title" value="<?php echo ($content["title"]); ?>"/>
        	<input type="hidden" name="review_id" value="0" />
        	<span class="avatar"><img src="<?php echo get_avatar(get_cookie('face'),30);?>" alt="" id="my_avatar"></span>
        	<div class="comment-bd" id="review">
        		<div class="comment-textarea">
					<textarea name="content" placeholder="我也来说两句..."></textarea>
				</div>

				<?php if(C('cfg_verify_review') == 1): ?><div class="comment-vcode">
					
					<input type="text" name="vcode" class="inp_small" />
					<img src="/index.php/Public/verify.html" id="VCode" onclick="javascript:changeVcode(this);" />
				</div><?php endif; ?>
			</div>
            <div class="comment-ft">
				<input type="submit" class="btn_blue" value="评论&nbsp;[&nbsp;Ctrl+Enter&nbsp;]">
			</div>
        </form>
        
        <div class="login-tip" style="display:none;">
            您可以选择 <a href="<?php echo U(GROUP_NAME. '/Public/login');?>">登录</a> | <a href="<?php echo U(GROUP_NAME. '/Public/register');?>">立即注册</a>
        </div>
    </div>
    <script type="text/javascript" src="__PUBLIC__/js/review.js"></script>
    <script type="text/javascript" language="javascript"> 
        var get_review_url = "<?php echo U(GROUP_NAME.'/Review/getlist');?>";
        var post_review_url = "<?php echo U(GROUP_NAME.'/Review/add');?>";   
		function changeVcode(obj){
			$("#VCode").attr("src",'/index.php/Public/verify.html'+ '#'+Math.random());
			return false;
		}
	</script><?php endif; ?>
    <!--comment end-->
    <div class=" clear"></div>
</div>


<!--
<div class="warp1 mt" id="bottom">
	<a href="http://127.0.0.1:1005">欢迎来到某某的公司</a>版权所有
	<br />
	联系地址：红牌楼  电话：18681378240<br />
	Copyright © 2014-2014 XYHCMS. 行云海软件 版权所有 <a href="http://www.0871k.com" target="_blank">Power by XYHCMS</a>
</div>
-->
<div id="footer">
    <a class="footer-gg"></a>
    <p>成都移客通科技有限公司 copyright (c) 2012-2013 备案号：蜀ICP备13018391号</p>
    <p>地址：成都武侯区武兴四路166号西部智谷D区 43幢405号 联系电话：028-85078440</p>
    <div class="footer-beian"><img src="__PUBLIC__/images/gongshang_badage.png" ></div>
</div>

</body>
</html>