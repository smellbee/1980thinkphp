﻿//var $ = jQuery.noConflict();
var optionHide={
		direction:"vertical",
		mode:"hide"
};
var optionShow={
		direction:"vertical",
		mode:"show",
		easing: 'easeInOutElastic'
};
var optionTransfer={
		to:"#sort_item_4 img",
		className:"jqui_transfer",
		easing: 'easeInOutExpo'
};
var optionSlideRight={
		direction:"right",
		mode:"show",
		easing: 'easeInOutExpo'
};
var optionSlideDown={
		direction:"down",
		mode:"hide",
		easing: 'easeInOutExpo'
};

var inFetching = false;

function randomOrder(targetArray){
	var arrayLength = targetArray.length;
  var tempArray1 = new Array();
	for (var i = 0; i < arrayLength; i ++){
		tempArray1 [i] = i;
	}
	var tempArray2 = new Array();
	for (var i = 0; i < arrayLength; i ++){
		tempArray2 [i] = tempArray1.splice (Math.floor (Math.random () * tempArray1.length) , 1);
	}
	var tempArray3 = new Array();
	for (var i = 0; i < arrayLength; i ++){
		tempArray3 [i] = targetArray [tempArray2 [i]];
	}
	return tempArray3;
}

function sortItems(jsonSortItems){
	this.selectId = jsonSortItems.selectId;
	this.sortItems = randomOrder(jsonSortItems.sortItems);
    //alert(this.sortItems.length);
	this.initImgs=function(){
		$("#item_title").hide();
		for(var i=0;i<this.sortItems.length;i++){
			var imgToShow = i==4?this.sortItems[i].imgBig:this.sortItems[i].imgSmall;
			$("#sort_item_"+i+" img").attr("src",imgToShow);
            $("#sort_item_"+i+" img").effect("clip", optionShow, 500)
			if(i!=4){
				$("#sort_item_"+i).bind("click",{itemId:i,switchImgs:this.switchImgs,imgsToSwitch:this.sortItems[i].imgBig,switchLink:this.sortItems[i].itemLink},function(event){
					event.data.switchImgs(event.data.itemId,event.data.imgsToSwitch,event.data.switchLink);
				});
			}else{
				$("#sort_item_link").attr("href",this.sortItems[i].itemLink);
				showTile(i);
			}
		}
	};
	this.fetchImgs=function(){
		if(inFetching){
			return;
		}else{
			inFetching = true;
		}
		this.sortItems = randomOrder(this.sortItems);
		$("#item_title").effect("slide",optionSlideDown,300);
		for(i=0;i<this.sortItems.length;i++){
			var timeDelay = parseInt(Math.random()*1000);
			var imgToShow = i==4?this.sortItems[i].imgBig:this.sortItems[i].imgSmall;
			var varPassToSt ={
					"fetchItemId":i,
					"itemImgToShow":imgToShow,
					"switchImgs":this.switchImgs,
					"switchLink":this.sortItems[i].itemLink,
					"imgBig":this.sortItems[i].imgBig
			};
			setTimeout(function(varPass){	
				$("#sort_item_"+varPass.fetchItemId+" img").effect("clip", optionHide, 200, function(){
						$("#sort_item_"+varPass.fetchItemId+" img").attr("src",varPass.itemImgToShow);
						$("#sort_item_"+varPass.fetchItemId+" img").effect("clip", optionShow, 500, function(){
							if(varPass.fetchItemId!=4){
								$("#sort_item_"+varPass.fetchItemId).unbind();
								$("#sort_item_"+varPass.fetchItemId).bind("click",{bindOption:varPass},function(event){
									event.data.bindOption.switchImgs(event.data.bindOption.fetchItemId, event.data.bindOption.imgBig, event.data.bindOption.switchLink);
								});
							}else{
								$("#sort_item_link").attr("href",varPass.switchLink);
								showTile(varPass.fetchItemId);
							}
						});
				});
			},timeDelay,varPassToSt);
		}
		setTimeout(function(){
			inFetching = false;
		},1500);
	};
	var _this = this;
	var showTile = function(itemId){	
		$("#item_title").html(_this.sortItems[itemId].itemTitle);
		$("#item_title").effect("slide",optionSlideRight,300);
	};
	
	this.switchImgs=function(itemId,imgToSwitch,switchLink){
		$("#item_title").effect("slide",optionSlideDown,300);
		$("#sort_item_"+itemId+" img").effect("transfer", optionTransfer, 500, function(){
			$("#sort_item_4 img").attr("src",imgToSwitch);
			$("#sort_item_link").attr("href",switchLink);
			showTile(itemId);
		});
		$(".jqui_transfer").css({
			"background":"url('"+ $("#sort_item_"+itemId+" img").attr("src") +"') 0 0 no-repeat",
			"background-size": "100%",
			"filter": "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='"+ $("#sort_item_"+itemId+" img").attr("src") +"', sizingMethod='scale')",
			"-ms-filter": "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='"+ $("#sort_item_"+itemId+" img").attr("src") +"', sizingMethod='scale')"
		});
	};
}
var page=1;
$(document).ready(function(){

    $.post("index.php?m=Show&a=pic",{page:page},function(data){
        var jsonSortInfo = data['data'][0];
        var sortItemsBunceShow = Array();
        sortItemsBunceShow= new sortItems(jsonSortInfo);
        sortItemsBunceShow.initImgs();
        $("#sort_tab_0").addClass("stabs_active");
    },"json");
    $("#sort_tab_0").click(function(){
        page++;
        $.post("index.php?m=Show&a=pic",{page:page},function(data){
            //console.log(data);
            if(data['msg']<9){
                page=0;
            }
            var jsonSortInfo = data['data'][0];
            var sortItemsBunceShow = Array();
            sortItemsBunceShow= new sortItems(jsonSortInfo);
            sortItemsBunceShow.fetchImgs();
        });
    });
});

