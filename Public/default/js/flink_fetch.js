var optionHide={
		direction:"vertical",
		mode:"hide"
};
var optionShow={
		direction:"vertical",
		mode:"show",
		easing: 'easeInOutElastic'
};
var optionSlideRight={
		direction:"right",
		mode:"show",
		easing: 'easeInOutExpo'
};
var optionSlideDown={
		direction:"down",
		mode:"hide",
		easing: 'easeInOutExpo'
};


function randomOrder(targetArray){
	var arrayLength = targetArray.length;
  var tempArray1 = new Array();
	for (var i = 0; i < arrayLength; i ++){
		tempArray1 [i] = i;
	}
	var tempArray2 = new Array();
	for (var i = 0; i < arrayLength; i ++){
		tempArray2 [i] = tempArray1.splice (Math.floor (Math.random () * tempArray1.length) , 1);
	}
	var tempArray3 = new Array();
	for (var i = 0; i < arrayLength; i ++){
		tempArray3 [i] = targetArray [tempArray2 [i]];
	}
	return tempArray3;
}


function flinkshow(containerId){
	this.currentpage=1;
	this.nextpagetoshow=2;
	this.resp=null;
	this.containerId=containerId;
	this.inFetching = false;
	this.itemFetched = 0;
	this.randomOrder = true;//是否在显示某一页的时候乱序显示, 不会改变分页的顺序
	
}

flinkshow.prototype.nextpage = function(){
	if(this.inFetching){
		return;
	}
	var that = this;
	this.inFetching = true;
	this.itemFetched = 0;
	if(that.randomOrder){
		that.resp.data.list = randomOrder(that.resp.data.list);
	}	
	$("#"+this.containerId).find("li").each(function(index){
		var timeDelay = parseInt(Math.random()*1000);
		var thisdom = this;
		setTimeout(function(){
			
			//补齐每页不足8个的情况
			var length = that.resp.data.list.length;
			if(length<8){
				for(var j = length;j<8;j++){
					var ekfly = {
						name:'移客通ekfly',
						image:'content/themes/default/images/sample/logo2.png',
						url:'http://www.163.com'
					};
					that.resp.data.list.push(ekfly);
				}
			}
			
			$(thisdom).find("p").effect("slide",optionSlideDown,300);
			$(thisdom).find("img").effect("clip", optionHide, 200,function(){//first we hide it
				$(thisdom).find("img").get(0).src=that.resp.data.list[index].image;
				$(thisdom).find("p").get(0).innerHTML=that.resp.data.list[index].name;
				$(thisdom).find("a").get(0).href=that.resp.data.list[index].url;
				$(thisdom).find("img").effect("clip", optionShow, 500,function(){
					$(thisdom).find("p").effect("slide",optionSlideRight,300);
					that.itemFetched++;
				});
			});
		},timeDelay);
	});

	this.finalizeFetching();
}

flinkshow.prototype.finalizeFetching = function(){
	if(!this.inFetching){//if we are not in fetching ,we do nothing
		return;
	}
	if(this.itemFetched < 8){//item fetching not finished
		var that=this;
		setTimeout(function(){
			that.finalizeFetching();
		},200);
	}else{//item fetching finished 
		this.currentpage = this.nextpagetoshow;
		this.nextpagetoshow++;
		if(this.nextpagetoshow > this.totalpage){
			this.nextpagetoshow = 1;//循环显示
		}
		this.inFetching = false;
		this.itemFetched = 0;
	}
}


$(document).ready(function(){
	var flinkBunceShow = new flinkshow("partner_msg");
	$("#partner_a").on("click",function(){		
		$.post("fetch_flinks",{page:flinkBunceShow.nextpagetoshow,size:8},function(data){
			flinkBunceShow.resp = data;
			flinkBunceShow.totalpage = Math.ceil(data['data']['total'] / 8);
			flinkBunceShow.nextpage();
		},"json");
	});
});
